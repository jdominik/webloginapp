package app.loginwebapp.test;
import java.io.Serializable;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;
import org.junit.Test;

import app.loginweb.entity.User;

public class MainAppTests {
	
	@Test
    public void crud() {
		
        SessionFactory sessionFactory = new AnnotationConfiguration().configure().buildSessionFactory();
        Session session = sessionFactory.openSession();
        User testUser = new User("test_user", "io");
        User testUser2 = new User("test_user2", "xyzXYZ1");
         
        Serializable createdId = create(session, testUser);
        read(session);
        Serializable createdId2 = create(session, testUser2);
        read(session);
         
        update(session, createdId);
        read(session);
         
        delete(session, createdId);
        read(session);
        delete(session, createdId2);
        read(session);
         
        session.close();
        
        // if no error - let's preconfigure app in other session
        Session sessionConfig = sessionFactory.openSession();
        User admin = new User("admin", "admin123");
        create(sessionConfig, admin);
        read(sessionConfig);
        sessionConfig.close();
        
    }
       
    private Serializable create(Session session, User u) {
        System.out.println("Creating user records...");
        session.beginTransaction();
        Serializable createdId = session.save(u);
        session.getTransaction().commit();
        //session.getTransaction().rollback(); // just for rollback test records in db
        return createdId;
    }
     
    private void update(Session session, Serializable idToUpdate) {
        System.out.println("Updating users...");
        User testUser = (User) session.get(User.class, idToUpdate);
        testUser.setName("test_user_updated");
        testUser.setPass("xyzXYZ1-/");
         
        session.beginTransaction();
        session.saveOrUpdate(testUser);
        session.getTransaction().commit();
    }
    
    private void delete(Session session, Serializable idToRemove) {
        System.out.println("Deleting user record...");
        User testUser = (User) session.get(User.class, idToRemove);
         
        session.beginTransaction();
        session.delete(testUser);
        session.getTransaction().commit();
    }

     
    private void read(Session session) {
        Query q = session.createQuery("select u from User u");
         
        List<User> users = q.list();
         
        System.out.println("Processing read of users...");
        System.out.printf("%-30.30s  %-30.30s%n", "UserName", "Password");
        
        users.stream()
        .forEach(u-> System.out.printf("%-30.30s  %-30.30s%n", u.getName(), u.getPass()));
        
    }

}
