package app.loginweb.entity;

import java.io.Serializable;

import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.servlet.http.HttpSession;

import app.loginweb.utils.AppUtils;
import app.loginweb.utils.DataDAO;


@Entity
@Table(name = "`U_USER`")
public class User implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="`ID`")
	private Long id;
	
	@Column(name="`USER_NAME`")
	private String name;
	
	@Column(name="`PASS`")
	private String pass;
	
	public User() {
		
	}
	
	public User(String name, String pass) {
		this.name = name;
		this.pass = pass;
	}
	
	//validate login
	public String validateUsernamePassword() {
		User user = DataDAO.findUser(name, pass);
		if (user != null) {
			HttpSession session = AppUtils.getSession();
			session.setAttribute("name", user);
			return "produkt";
		} else {
			FacesContext.getCurrentInstance().addMessage(
					null,
					new FacesMessage(FacesMessage.SEVERITY_WARN,
							"Incorrect Username and Password",
							"Please enter correct username and Password"));
			return "login";
		}
	}

	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", name=" + name + ", pass=" + pass + "]";
	}
	
}
