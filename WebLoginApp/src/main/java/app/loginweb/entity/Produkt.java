package app.loginweb.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;


@Entity
@Table(name="`P_PRODUKT`")
public class Produkt implements Serializable {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 2L;

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name="`ID`")
	private Long id;
	
	@Column(name="`NAZWA`")
	private String nazwa;
	
	@Column(name="`CENA`")
	private Long cena;	// price as 'grosze' just to represent it later as zloty and grosz
	
	@Transient
	private boolean editable;
	
	@Transient
	private Float cenaForView;
	
	public Produkt() {
		
	}
	
	public Produkt(String nazwa, Long cena) {
		this.nazwa = nazwa;
		this.cena = cena;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNazwa() {
		return nazwa;
	}

	public void setNazwa(String nazwa) {
		this.nazwa = nazwa;
	}

	public Long getCena() {
		return cena;
	}

	public void setCena(Long cena) {
		this.cena = cena;
	}

	public boolean isEditable() {
		return editable;
	}

	public void setEditable(boolean editable) {
		this.editable = editable;
	}

	public Float getCenaForView() {
		if (this.cena != null) {
			return Float.valueOf(this.cena)/100F;
		}
		return null;
	}

	public void setCenaForView(Float cenaForView) {
		if (cenaForView != null) {
			Float f = cenaForView * 100F;
			this.cena = f.longValue();
		}
	}
	
	@Override
	public String toString() {
		return "Produkt [" + getNazwa() + " cena=" + getCenaForView() + "]";
	}

}
