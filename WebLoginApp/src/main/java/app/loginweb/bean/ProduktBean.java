package app.loginweb.bean;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;

import app.loginweb.entity.Produkt;
import app.loginweb.utils.DataDAO;

@ManagedBean(name="produktbean")
@SessionScoped
public class ProduktBean implements Serializable {
	
	private static final long serialVersionUID = 2282801825228686366L;
	private List<Produkt> produktList;
	private boolean sortAsc = true;
	
	public ProduktBean() {
		this.produktList = initProduktList();
	}
	
	public List<Produkt> getProduktList() {
		return this.produktList;
	}
	
	public List<Produkt> initProduktList() {
		try {
			produktList = DataDAO.selectProduktList();
			return produktList;
		} catch (Exception e) {
			System.out.print("Unable to select produkt, details: " + e.getMessage());
			return new ArrayList<Produkt>();
		}
	}
	
	public void setProduktList(List<Produkt> produktList) {
		this.produktList = produktList;
	}

	
	public boolean createProdukt(Produkt produkt) {
		try {
			DataDAO.saveProdukt(produkt);
		} catch (Exception e) {
			System.out.print("Unable to create produkt, details: " + e.getMessage());
			return false;
		}
		return true;
	}
	
	public String saveAction() {
		List<Produkt> produktForUpdateList = new ArrayList<>();
        //get all existing value but set "editable" to false 
        for (Produkt produkt : produktList){
        	if (produkt.isEditable()) {
        		if (produkt.getCena() != null && produkt.getCena() > 0 && produkt.getNazwa() != "") {
        			produktForUpdateList.add(produkt);
        		} else {
        			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
        					"Podaj prawidowe wartości w polach cena i nazwa! Zapis nie powiódł się", "Niepoprawna cena lub nazwa"));
        			return null;
        		}
        	}
        }
        for (Produkt produktForUpdate : produktForUpdateList) {
        	DataDAO.saveProdukt(produktForUpdate);
        	produktForUpdate.setEditable(false);
        }
        
        //return to current page
        return null;
    }
    
    public String editAction(Produkt produkt) {
        
    	produkt.setEditable(true);
        return null;
    }
    
    public String addAction() {
    	Produkt produkt = new Produkt();
    	if (produktList == null) {
    		produktList = initProduktList();
    	}
    	produktList.add(produkt);
    	editAction(produkt);
        return null;
    }
    
    public String deleteAction(Produkt produkt) {
    	DataDAO.deleteProdukt(produkt);
    	produktList.remove(produkt);
        return null;
    }
    
    public String orderByAction(final OrderField order) {
    	final boolean sort = this.sortAsc;
    	Collections.sort(produktList, new Comparator<Produkt>() {
    		
			@Override
			public int compare(Produkt p1, Produkt p2) {
				switch(order) {
	    		case NAZWA:
	    			return sort ? p1.getNazwa().compareTo(p2.getNazwa()) : p2.getNazwa().compareTo(p1.getNazwa());
	    		case CENA:
	    			return sort ? p1.getCena().compareTo(p2.getCena()) : p2.getCena().compareTo(p1.getCena());
				}
				return 0;
			}
    		
    	});
    	this.sortAsc = !sort;
    	return null;
    }
    
    private enum OrderField {
    	NAZWA, CENA;
    }

}