package app.loginweb.bean;

import java.io.Serializable;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpSession;

import app.loginweb.entity.User;
import app.loginweb.utils.AppUtils;
import app.loginweb.utils.DataDAO;

@ManagedBean(name="loginbean")
@SessionScoped
public class LoginBean implements Serializable {
	
	private static final long serialVersionUID = 1294801825228386363L;
	private User user;

	public LoginBean() {
		this.user = new User();
	}

	public LoginBean(User user) {
		this.user = user;
	}


	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	

	// validate login
	public String validateUsernamePassword() {
		boolean valid = DataDAO.validate(user.getName(), user.getPass());
		if (valid) {
			HttpSession session = AppUtils.getSession();
			session.setAttribute("usersession", user.getName());
			return "protected/produkt";
		} else {
			FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_WARN,
					"Nieprawidłowa nazwa lub hasło", "Podaj prawidłowe dane logowania"));
			return "login";
		}
	}

}
