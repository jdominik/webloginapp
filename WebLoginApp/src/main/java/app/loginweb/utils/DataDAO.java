package app.loginweb.utils;


import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

import app.loginweb.entity.Produkt;
import app.loginweb.entity.User;

public class DataDAO {
	

	private static final SessionFactory sessionFactory = new AnnotationConfiguration().configure()
			.buildSessionFactory();
	
	
	public static List<Produkt> selectProduktList() {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		Query q = session.createQuery("FROM Produkt p");
		//q.setParameter("pass", password);
		//q.setParameter("user_name", userName);
		List<Produkt> produktList = q.list();
		session.close();
		return produktList;
	}
	
	public static boolean saveProdukt(Produkt produkt) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.saveOrUpdate(produkt);
		session.getTransaction().commit();
		return false;
	}
	
	public static boolean deleteProdukt(Produkt produkt) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();
		session.delete(produkt);
		session.getTransaction().commit();
		return false;
	}

	public static User findUser(String userName, String password) {
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Query q = session.createQuery("FROM U_USER user where user.PASS = :pass AND user.USER_NAME = :user_name ");
		q.setParameter("pass", password);
		q.setParameter("user_name", userName);
		User user = (User) q.uniqueResult();
		session.close();
		return user;
	}

	public static boolean validate(String userName, String password) {
		System.out.print(userName + " " + password);
		Session session = sessionFactory.openSession();
		session.beginTransaction();

		Query q = session.createQuery("FROM User u where u.pass = :pass AND u.name = :userName");
		q.setParameter("pass", password);
		q.setParameter("userName", userName);
		User user = (User) q.uniqueResult();
		session.close();
		System.out.print(user);
		if (user != null)
			return true;
		else
			return false;
	}

}
